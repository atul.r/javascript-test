// Create a doubly linked list. 
// Write functions for inserting and iterating in linked list. 
// Use js various structures to implement the same
class DoublyLinkedListNode {
    constructor(data) {
        this.data = data;
        this.next = null;
        this.previous = null;
    }

}

const head = new DoublyLinkedListNode(10);


const secondNode = new DoublyLinkedListNode(20);
head.next = secondNode;
secondNode.previous = head;


const thirdNode = new DoublyLinkedListNode(30);
secondNode.next = thirdNode;
thirdNode.previous = secondNode;




// Entering new node

let newNodeData=40;
const fourthNode = new DoublyLinkedListNode(newNodeData);
thirdNode.next=fourthNode;
fourthNode.previous=thirdNode;

//iterating
let current=head;
const tail=fourthNode;
while(current!=null)
{
console.log(current.data);
current=current.next;
}
